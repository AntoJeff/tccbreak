﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Cardapio
{
    class CardapioDTO
    {
        public int Id_cardapio { get; set; }
        public string Produto { get; set; }
        public decimal Valor { get; set; }
        public string Tipo { get; set; }


    }
}
