﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pagamento
{
    class PagamentoDatabase
    {
        public int Salvar(PagamentoDTO dto)
        {
            string script = @"insert into tb_pagamento(ds_forma_pagamento,ds_parcelas,ds_cpf_nota,id_pedido,id_cliente_pedido) 
             values(@ds_forma_pagamento,@ds_parcelas,@ds_cpf_nota,@id_pedido,@id_cliente_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_forma_pagamento", dto.Forma_pagamento));
            parms.Add(new MySqlParameter("ds_parcelas", dto.Parcelas));
            parms.Add(new MySqlParameter("ds_cpf_nota", dto.Cpf_nota));
            parms.Add(new MySqlParameter("id_pedido", dto.Id_pedido));
            parms.Add(new MySqlParameter("id_cliente_pedido", dto.Id_pedido));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }




    
    }
}
