﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pagamento
{
    class PagamentoDTO
    {
        public int Id_pagamento { get; set; }
        public string Forma_pagamento { get; set; }
        public string Parcelas { get; set; }
        public int Cpf_nota { get; set; }
        public int Id_pedido { get; set; }
        public int Id_cliente { get; set; }
    }
}
