﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes
{
    class EstoqueDTO
    {
        public int Id_estoque { get; set; }
        public int Id_cardapio { get; set; }
        public double Quantidade { get; set; }
        public decimal Valor { get; set; }
        public string Fornecedor { get; set; }

    }
    
}
