﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Estoque
{
    class EstoqueBusiness
    {
        public int Salvar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Salvar(dto);
        }
        public List<EstoqueDTO> Listar()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar();
        }
        public void Remover(int id_estoque)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Remover(id_estoque);
        } 
        public int Baixa(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Baixa(dto);
        } 
        public List<EstoqueView> View()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.View();
        }
    }
}
