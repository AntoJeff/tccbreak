﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script = @"insert into tb_estoque (id_cardapio,nr_quantidade,vl_valor,nm_Fornecedor)
                                    values (@id_cardapio,@nr_quantidade,@vl_valor,@nm_Fornecedor)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cardapio",dto.Id_cardapio));
            parms.Add(new MySqlParameter("nr_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("vl_valor",dto.Valor));
            parms.Add(new MySqlParameter("nm_Fornecedor", dto.Valor));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<EstoqueDTO> Listar()
        {
            string script = @"select * from tb_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Id_estoque = reader.GetInt32("id_estoque");
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Quantidade = reader.GetInt32("nr_quantidade");
                dto.Valor = reader.GetDecimal("vl_valor");
                dto.Fornecedor = reader.GetString("nm_Fornecedor");
                lista.Add(dto);

            }
            reader.Close();
            return lista;
        }
        public void Remover(int id_estoque)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", id_estoque));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    public int Baixa(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque
                                     SET id_cardapio      = @id_cardapio,
                                         nr_quantidade    = @nr_quantidade,
                                         vl_valor         = @vl_valor,
                                         nm_Fornecedor    = @nm_Fornecedor
                                       WHERE id_estoque   = @id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", dto.Id_estoque));
            parms.Add(new MySqlParameter("id_cardapio", dto.Id_cardapio));
            parms.Add(new MySqlParameter("nr_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("vl_valor", dto.Valor));
            parms.Add(new MySqlParameter("nm_Fornecedor", dto.Valor));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

        public List<EstoqueView> View()
        {
            string script = @"select * from vw_consulta";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EstoqueView> lista = new List<EstoqueView>();
            while (reader.Read())
            {
                EstoqueView dto = new EstoqueView();
                //dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Produto = reader.GetString("produto");
                dto.Quantidade = reader.GetInt32("quantidade");
                dto.Valor = reader.GetDecimal("valor"); 
                lista.Add(dto);

            }
            reader.Close();
            return lista;

        }
    }
}
