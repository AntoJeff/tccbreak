﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoDTO
    {
        public int Id_pedido { get; set; }
        public string Produto { get; set; }
        public int Quantidade { get; set; }
        public string Observacao { get; set; }
        public int Telefone { get; set; }
        public int Id_cardapio_pedido { get; set; }



    }
}
