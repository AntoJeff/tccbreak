﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Cargo
{
    class CargoBusiness
    {
        public List<CargoDTO> Listar()
        {
            CargoDatabase db = new CargoDatabase();
            return db.Listar();
        }
    }
}
