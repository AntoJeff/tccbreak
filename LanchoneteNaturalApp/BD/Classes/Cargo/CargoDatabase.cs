﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Cargo
{
    class CargoDatabase
    {
        public List<CargoDTO> Listar()
        {
            string script = @"insert into tb_cargo(ds_cargo,vl_salario,ds_horario) 
                               values (@ds_cargo,@vl_salario,@ds_horario)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<CargoDTO> lista = new List<CargoDTO>();
            while (reader.Read())
            {
                CargoDTO dto = new CargoDTO();
                dto.Id_cargo = reader.GetInt32("id_cargo");
                dto.Horario = reader.GetInt32("ds_horario");
                dto.Vl_cargo = reader.GetInt32("vl_salario");
                dto.Cargo = reader.GetString("ds_cargo");
                lista.Add(dto);

            }
            reader.Close();
            return lista;

        }
    }
}
