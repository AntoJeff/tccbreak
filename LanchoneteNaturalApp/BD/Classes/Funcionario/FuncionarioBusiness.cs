﻿using LanchoneteNaturalApp.BD.Classes.Funcionarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Funcionario
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionariosDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);
        }
        public List<FuncionariosDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        } 
        public void Alterar(FuncionariosDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(dto);
        }
        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);
        }
    }
}
