﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Funcionarios
{
    class FuncionariosDTO
    {
        public int Id_funcionario { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public string Rg { get; set; }
        public int Cpf { get; set; }
        public string Endereco { get; set; }
        public int Telefone { get; set; }
        public string Complemento { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public string Cargo { get; set; }
        public float Salario { get; set; }
        public int Hora { get; set; }



    }
}
