﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Cliente
{
    class ClienteBusiness
    { 
        public int Salvar(ClienteDTO dto)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        } 
        public List<ClienteDTO> Consulta()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.consultar();
        } 
        public void Alterar(ClienteDTO dto)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Alterar(dto);
        }
        public void Remover(int id_cliente)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Remover(id_cliente);
        }
    }
}
