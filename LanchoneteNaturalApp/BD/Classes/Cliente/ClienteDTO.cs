﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Cliente
{
    class ClienteDTO
    {

        public int Id_cliente { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public int Cep { get; set; }
        public int Telefone { get; set; }
        public int Celular { get; set; }
        public int Cpf { get; set; }
        public string Sobrenome { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public string Email { get; set; }








    }
}
