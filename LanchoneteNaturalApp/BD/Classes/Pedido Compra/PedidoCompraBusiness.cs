﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoCompraBusiness
    {
        public int Salvar(PedidoCompraDTO dto)
        {
            PedidoCompraDatabase db = new PedidoCompraDatabase();
            return db.Salvar(dto);
        }
        public List<PedidoCompraDTO> Listar()
        {
            PedidoCompraDatabase db = new PedidoCompraDatabase();
            return db.Listar();
        }
    }
}
