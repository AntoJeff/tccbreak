﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoCompraDatabase
    {
        public int Salvar(PedidoCompraDTO dto)
        {
            string script = @"insert into tb_pedido_item(id_cardapio,id_cliente) 
                                          values(@id_cardapio,@id_cliente) ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cardapio", dto.Id_cardapio));
            parms.Add(new MySqlParameter("id_cliente", dto.Id_cliente));
            

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PedidoCompraDTO> Listar()
        {
            string script = @"select * from tb_compra_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<PedidoCompraDTO> lista = new List<PedidoCompraDTO>();
            while (reader.Read())
            {
                PedidoCompraDTO dto = new PedidoCompraDTO();
                dto.Id_pedido_item = reader.GetInt32("id_pedido_item");
                dto.Id_cliente = reader.GetInt32("id_cliente");
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }
        
    }
}
