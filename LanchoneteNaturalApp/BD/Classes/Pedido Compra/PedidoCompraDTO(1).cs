﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoCompraDTO
    {
        public int Id_compra_produto { get; set; }
        public string Fornecedor { get; set; }
        public string Produto { get; set; }
        public int Quantidade { get; set; }
        public int Id_fornecedor_comprar { get; set; }



    }
}
