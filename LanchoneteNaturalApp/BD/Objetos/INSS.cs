﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Objetos
{
    class INSS
    {
        public float Inss(float cargo)
        {
            if (cargo <= 1.662f)
            {
                float inss1 = cargo * 0.08f;
                return inss1;
            }
            if (cargo >=1.663f)
            {
                float inss2 = cargo * 0.09f;
                return inss2;
            }
            //if (cargo >= 4.000f)
            //{
            //    float inss3 = cargo * 0.011f;
            //    return inss3;
            //}
            return cargo;
        }

    }
}
