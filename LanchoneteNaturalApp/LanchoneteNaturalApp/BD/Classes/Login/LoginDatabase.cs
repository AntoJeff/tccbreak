﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes
{
    class LoginDatabase
    {
        public bool Logar(string usuario, string senha)
        {
            string script = @"SELECT * FROM tb_cadastro WHERE usuario = @usuario AND senha = @senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("usuario", usuario));
            parms.Add(new MySqlParameter("senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            bool logou = false;
            if (reader.Read())
            {
                logou = true;
            }
            else
            {
                logou = false;
            }
            reader.Close();
            return logou;
        }
    }
}
