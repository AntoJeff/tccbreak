﻿namespace LanchoneteNaturalApp.Telas.Frms
{
    partial class FrmTelaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTelaPrincipal));
            this.btnfuncionario = new System.Windows.Forms.Button();
            this.btncompraproduto = new System.Windows.Forms.Button();
            this.btnfornecedor = new System.Windows.Forms.Button();
            this.btnfuncionarioconsulta = new System.Windows.Forms.Button();
            this.btnLogoff = new System.Windows.Forms.Button();
            this.btnbaixaestoque = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Pagamentos = new System.Windows.Forms.Button();
            this.btnconsultafornecedor = new System.Windows.Forms.Button();
            this.bntestoque = new System.Windows.Forms.Button();
            this.btncliente = new System.Windows.Forms.Button();
            this.bntconsultacliente = new System.Windows.Forms.Button();
            this.btnconsultapedido = new System.Windows.Forms.Button();
            this.btncardapio = new System.Windows.Forms.Button();
            this.btnpedidocliente = new System.Windows.Forms.Button();
            this.pnlConteudo = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lbltitulo = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lblLogo = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlConteudo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnfuncionario
            // 
            this.btnfuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfuncionario.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfuncionario.ForeColor = System.Drawing.Color.Black;
            this.btnfuncionario.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnfuncionario.Location = new System.Drawing.Point(28, 122);
            this.btnfuncionario.Name = "btnfuncionario";
            this.btnfuncionario.Size = new System.Drawing.Size(116, 33);
            this.btnfuncionario.TabIndex = 25;
            this.btnfuncionario.Text = "Funcionario";
            this.btnfuncionario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfuncionario.UseVisualStyleBackColor = true;
            this.btnfuncionario.Click += new System.EventHandler(this.btnFuncionario_Click);
            // 
            // btncompraproduto
            // 
            this.btncompraproduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btncompraproduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncompraproduto.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncompraproduto.ForeColor = System.Drawing.Color.Black;
            this.btncompraproduto.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btncompraproduto.Location = new System.Drawing.Point(28, 356);
            this.btncompraproduto.Name = "btncompraproduto";
            this.btncompraproduto.Size = new System.Drawing.Size(116, 33);
            this.btncompraproduto.TabIndex = 28;
            this.btncompraproduto.Text = "Comprar Prod";
            this.btncompraproduto.UseVisualStyleBackColor = true;
            this.btncompraproduto.Click += new System.EventHandler(this.BtnOrdemDePedido_Click);
            // 
            // btnfornecedor
            // 
            this.btnfornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfornecedor.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfornecedor.ForeColor = System.Drawing.Color.Black;
            this.btnfornecedor.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnfornecedor.Location = new System.Drawing.Point(28, 278);
            this.btnfornecedor.Name = "btnfornecedor";
            this.btnfornecedor.Size = new System.Drawing.Size(116, 33);
            this.btnfornecedor.TabIndex = 29;
            this.btnfornecedor.Text = "Fornecedor";
            this.btnfornecedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfornecedor.UseVisualStyleBackColor = true;
            this.btnfornecedor.Click += new System.EventHandler(this.BtnFornecedor_Click);
            // 
            // btnfuncionarioconsulta
            // 
            this.btnfuncionarioconsulta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnfuncionarioconsulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfuncionarioconsulta.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfuncionarioconsulta.ForeColor = System.Drawing.Color.Black;
            this.btnfuncionarioconsulta.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnfuncionarioconsulta.Location = new System.Drawing.Point(28, 161);
            this.btnfuncionarioconsulta.Name = "btnfuncionarioconsulta";
            this.btnfuncionarioconsulta.Size = new System.Drawing.Size(116, 33);
            this.btnfuncionarioconsulta.TabIndex = 32;
            this.btnfuncionarioconsulta.Text = "Cons. Func";
            this.btnfuncionarioconsulta.UseVisualStyleBackColor = true;
            this.btnfuncionarioconsulta.Click += new System.EventHandler(this.BtnConsFunc_Click);
            // 
            // btnLogoff
            // 
            this.btnLogoff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogoff.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogoff.ForeColor = System.Drawing.Color.Black;
            this.btnLogoff.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnLogoff.Location = new System.Drawing.Point(30, 551);
            this.btnLogoff.Name = "btnLogoff";
            this.btnLogoff.Size = new System.Drawing.Size(116, 33);
            this.btnLogoff.TabIndex = 33;
            this.btnLogoff.Text = "Logoff";
            this.btnLogoff.UseVisualStyleBackColor = true;
            this.btnLogoff.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnbaixaestoque
            // 
            this.btnbaixaestoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnbaixaestoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbaixaestoque.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbaixaestoque.ForeColor = System.Drawing.Color.Black;
            this.btnbaixaestoque.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnbaixaestoque.Location = new System.Drawing.Point(28, 395);
            this.btnbaixaestoque.Name = "btnbaixaestoque";
            this.btnbaixaestoque.Size = new System.Drawing.Size(116, 33);
            this.btnbaixaestoque.TabIndex = 35;
            this.btnbaixaestoque.Text = "Baixa Estoque";
            this.btnbaixaestoque.UseVisualStyleBackColor = true;
            this.btnbaixaestoque.Click += new System.EventHandler(this.btnCPedido_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Orange;
            this.panel1.Controls.Add(this.Pagamentos);
            this.panel1.Controls.Add(this.btnconsultafornecedor);
            this.panel1.Controls.Add(this.bntestoque);
            this.panel1.Controls.Add(this.btncliente);
            this.panel1.Controls.Add(this.bntconsultacliente);
            this.panel1.Controls.Add(this.btnconsultapedido);
            this.panel1.Controls.Add(this.btncardapio);
            this.panel1.Controls.Add(this.btnpedidocliente);
            this.panel1.Controls.Add(this.btnbaixaestoque);
            this.panel1.Controls.Add(this.btnLogoff);
            this.panel1.Controls.Add(this.btnfuncionarioconsulta);
            this.panel1.Controls.Add(this.btnfornecedor);
            this.panel1.Controls.Add(this.btncompraproduto);
            this.panel1.Controls.Add(this.btnfuncionario);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(172, 605);
            this.panel1.TabIndex = 28;
            // 
            // Pagamentos
            // 
            this.Pagamentos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Pagamentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pagamentos.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pagamentos.ForeColor = System.Drawing.Color.Black;
            this.Pagamentos.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.Pagamentos.Location = new System.Drawing.Point(30, 512);
            this.Pagamentos.Name = "Pagamentos";
            this.Pagamentos.Size = new System.Drawing.Size(116, 33);
            this.Pagamentos.TabIndex = 43;
            this.Pagamentos.Text = "Pagamento";
            this.Pagamentos.UseVisualStyleBackColor = true;
            this.Pagamentos.Click += new System.EventHandler(this.Pagamentos_Click);
            // 
            // btnconsultafornecedor
            // 
            this.btnconsultafornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnconsultafornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnconsultafornecedor.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnconsultafornecedor.ForeColor = System.Drawing.Color.Black;
            this.btnconsultafornecedor.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnconsultafornecedor.Location = new System.Drawing.Point(28, 317);
            this.btnconsultafornecedor.Name = "btnconsultafornecedor";
            this.btnconsultafornecedor.Size = new System.Drawing.Size(116, 33);
            this.btnconsultafornecedor.TabIndex = 42;
            this.btnconsultafornecedor.Text = "Cons. For";
            this.btnconsultafornecedor.UseVisualStyleBackColor = true;
            this.btnconsultafornecedor.Click += new System.EventHandler(this.btnconsultafornecedor_Click);
            // 
            // bntestoque
            // 
            this.bntestoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntestoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bntestoque.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntestoque.ForeColor = System.Drawing.Color.Black;
            this.bntestoque.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bntestoque.Location = new System.Drawing.Point(28, 239);
            this.bntestoque.Name = "bntestoque";
            this.bntestoque.Size = new System.Drawing.Size(116, 33);
            this.bntestoque.TabIndex = 41;
            this.bntestoque.Text = "Estoque";
            this.bntestoque.UseVisualStyleBackColor = true;
            this.bntestoque.Click += new System.EventHandler(this.bntestoq_Click);
            // 
            // btncliente
            // 
            this.btncliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btncliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncliente.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncliente.ForeColor = System.Drawing.Color.Black;
            this.btncliente.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btncliente.Location = new System.Drawing.Point(28, 44);
            this.btncliente.Name = "btncliente";
            this.btncliente.Size = new System.Drawing.Size(116, 33);
            this.btncliente.TabIndex = 40;
            this.btncliente.Text = "Cliente";
            this.btncliente.UseVisualStyleBackColor = true;
            this.btncliente.Click += new System.EventHandler(this.btncliente_Click);
            // 
            // bntconsultacliente
            // 
            this.bntconsultacliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntconsultacliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bntconsultacliente.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntconsultacliente.ForeColor = System.Drawing.Color.Black;
            this.bntconsultacliente.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bntconsultacliente.Location = new System.Drawing.Point(28, 83);
            this.bntconsultacliente.Name = "bntconsultacliente";
            this.bntconsultacliente.Size = new System.Drawing.Size(116, 33);
            this.bntconsultacliente.TabIndex = 39;
            this.bntconsultacliente.Text = "Cons. Cli";
            this.bntconsultacliente.UseVisualStyleBackColor = true;
            this.bntconsultacliente.Click += new System.EventHandler(this.bntconsultacliente_Click);
            // 
            // btnconsultapedido
            // 
            this.btnconsultapedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnconsultapedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnconsultapedido.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnconsultapedido.ForeColor = System.Drawing.Color.Black;
            this.btnconsultapedido.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnconsultapedido.Location = new System.Drawing.Point(30, 473);
            this.btnconsultapedido.Name = "btnconsultapedido";
            this.btnconsultapedido.Size = new System.Drawing.Size(116, 33);
            this.btnconsultapedido.TabIndex = 38;
            this.btnconsultapedido.Text = "Cons. Ped";
            this.btnconsultapedido.UseVisualStyleBackColor = true;
            this.btnconsultapedido.Click += new System.EventHandler(this.button4_Click);
            // 
            // btncardapio
            // 
            this.btncardapio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btncardapio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncardapio.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncardapio.ForeColor = System.Drawing.Color.Black;
            this.btncardapio.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btncardapio.Location = new System.Drawing.Point(28, 200);
            this.btncardapio.Name = "btncardapio";
            this.btncardapio.Size = new System.Drawing.Size(116, 33);
            this.btncardapio.TabIndex = 37;
            this.btncardapio.Text = "Cardapio";
            this.btncardapio.UseVisualStyleBackColor = true;
            this.btncardapio.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // btnpedidocliente
            // 
            this.btnpedidocliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnpedidocliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpedidocliente.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpedidocliente.ForeColor = System.Drawing.Color.Black;
            this.btnpedidocliente.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnpedidocliente.Location = new System.Drawing.Point(28, 434);
            this.btnpedidocliente.Name = "btnpedidocliente";
            this.btnpedidocliente.Size = new System.Drawing.Size(116, 33);
            this.btnpedidocliente.TabIndex = 36;
            this.btnpedidocliente.Text = "Pedido Cli";
            this.btnpedidocliente.UseVisualStyleBackColor = true;
            this.btnpedidocliente.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // pnlConteudo
            // 
            this.pnlConteudo.BackColor = System.Drawing.Color.Bisque;
            this.pnlConteudo.BackgroundImage = global::LanchoneteNaturalApp.Properties.Resources.oie_transparent;
            this.pnlConteudo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlConteudo.Controls.Add(this.label2);
            this.pnlConteudo.ForeColor = System.Drawing.Color.Black;
            this.pnlConteudo.Location = new System.Drawing.Point(172, 41);
            this.pnlConteudo.Name = "pnlConteudo";
            this.pnlConteudo.Size = new System.Drawing.Size(739, 560);
            this.pnlConteudo.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Poor Richard", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Location = new System.Drawing.Point(251, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 36);
            this.label2.TabIndex = 34;
            this.label2.Text = "Bem Vindo";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbltitulo
            // 
            this.lbltitulo.AutoSize = true;
            this.lbltitulo.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltitulo.Location = new System.Drawing.Point(415, 20);
            this.lbltitulo.Name = "lbltitulo";
            this.lbltitulo.Size = new System.Drawing.Size(63, 24);
            this.lbltitulo.TabIndex = 43;
            this.lbltitulo.Text = "label1";
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.button3.Image = global::LanchoneteNaturalApp.Properties.Resources.icons8_delete_30;
            this.button3.Location = new System.Drawing.Point(622, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(44, 43);
            this.button3.TabIndex = 31;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_2);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Orange;
            this.panel2.Controls.Add(this.lblLogo);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Location = new System.Drawing.Point(169, -14);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(670, 55);
            this.panel2.TabIndex = 30;
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Orange;
            this.button1.Location = new System.Drawing.Point(574, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 43);
            this.button1.TabIndex = 34;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_4);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Orange;
            this.button2.Image = global::LanchoneteNaturalApp.Properties.Resources.icons8_delete_30;
            this.button2.Location = new System.Drawing.Point(624, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(44, 43);
            this.button2.TabIndex = 32;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_3);
            // 
            // lblLogo
            // 
            this.lblLogo.AutoSize = true;
            this.lblLogo.Font = new System.Drawing.Font("Poor Richard", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogo.ForeColor = System.Drawing.Color.Black;
            this.lblLogo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblLogo.Location = new System.Drawing.Point(-4, 24);
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Size = new System.Drawing.Size(125, 31);
            this.lblLogo.TabIndex = 35;
            this.lblLogo.Text = "Break Bio";
            this.lblLogo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmTelaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(838, 598);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlConteudo);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmTelaPrincipal";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmTelaPrincipal_Load);
            this.panel1.ResumeLayout(false);
            this.pnlConteudo.ResumeLayout(false);
            this.pnlConteudo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnfuncionario;
        private System.Windows.Forms.Button btncompraproduto;
        private System.Windows.Forms.Button btnfornecedor;
        private System.Windows.Forms.Button btnfuncionarioconsulta;
        private System.Windows.Forms.Button btnLogoff;
        private System.Windows.Forms.Button btnbaixaestoque;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btncardapio;
        private System.Windows.Forms.Button btnpedidocliente;
        private System.Windows.Forms.Button btnconsultapedido;
        private UserControls.UCPedidoCliente ucPedidoCliente2;
        private System.Windows.Forms.Button btncliente;
        private System.Windows.Forms.Button bntconsultacliente;
        private UserControls.UCPedidoComprar PedidoComprar;
        private UserControls.UCPedidoCliente PedidoCliente;
        private UserControls.UCFornecedor Fornecedor;
        private UserControls.UCEstoque Estoque;
        private UserControls.UCConsultarPedidos ConsultarPedidos;
        private UserControls.UCConsultaFuncionario ConsultaFuncionario;
        private UserControls.UCConsultaCliente ConsultaCliente;
        private UserControls.UCCadastrofuncionario Cadastrofuncionario;
        private UserControls.UCCadastroCliente CadastroCliente;
        private UserControls.UCCadastroCardapio CadastroCardapio;
        private UserControls.UCBaixaEstoque BaixaEstoque;
        private System.Windows.Forms.Button bntestoque;
        private System.Windows.Forms.Label lbltitulo;
        private System.Windows.Forms.Panel pnlConteudo;
        private System.Windows.Forms.Button btnconsultafornecedor;
        private System.Windows.Forms.Button Pagamentos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblLogo;
    }
}