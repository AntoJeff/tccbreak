﻿using LanchoneteNaturalApp.Telas.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LanchoneteNaturalApp.Telas.Frms
{
    public partial class FrmTelaPrincipal : Form
    {
        public FrmTelaPrincipal()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
             
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFuncionario_Click(object sender, EventArgs e)
        {
            UCCadastrofuncionario tela = new UCCadastrofuncionario();
            OpenScreen(tela);
            lblLogo.Text = "Cadastro de Funcionario";
        }

        private void button3_Click_2(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ucCompraProdutoEst1_Load(object sender, EventArgs e)
        {

        }

        private void Cad_Cliente_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnCompra_Click(object sender, EventArgs e)
        {

        }



        private void button2_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnConsFunc_Click(object sender, EventArgs e)
        {
            UCConsultaFuncionario tela = new UCConsultaFuncionario();
            OpenScreen(tela);
            lblLogo.Text = "Consulta de Funcionario";

        }

        private void btnEstoque_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnFornecedor_Click(object sender, EventArgs e)
        {
            UCFornecedor tela = new UCFornecedor();
            OpenScreen(tela);
            lblLogo.Text = "Cadastro de Fornecedor";
        }

        private void BtnPagamento_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnOrdemDePedido_Click(object sender, EventArgs e)
        {
            UCPedidoComprar tela = new UCPedidoComprar();
            OpenScreen(tela);
            lblLogo.Text = "Pedido de Compra";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FrmLogin login = new FrmLogin();
            login.Show();
            this.Hide();
        }

        private void ucPagamento1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
          
        }

        private void btnCPedido_Click(object sender, EventArgs e)
        {
            UCBaixaEstoque tela = new UCBaixaEstoque();
            OpenScreen(tela);
            lblLogo.Text = "Baixa de Estoque";
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            UCPedidoCliente tela = new UCPedidoCliente();
            OpenScreen(tela);
            lblLogo.Text = "Ordem de Pedidos";


        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            UCCadastroCardapio tela = new UCCadastroCardapio();
            OpenScreen(tela);
            lblLogo.Text = "Cadastro de Produtos";
       }

        private void button4_Click(object sender, EventArgs e)
        {
            UCConsultarPedidos tela = new UCConsultarPedidos();
            OpenScreen(tela);
            lblLogo.Text = "Consulta de Pedidos";
        }

        private void btncliente_Click(object sender, EventArgs e)
        {
            UCCadastroCliente tela = new UCCadastroCliente();
            OpenScreen(tela);
            lblLogo.Text = "Cadastro de Clientes";
        }

        private void bntconsultacliente_Click(object sender, EventArgs e)
        {
            UCConsultaCliente tela = new UCConsultaCliente();
            OpenScreen(tela);
            lblLogo.Text = "Consulta de Clientes";
        }

        private void bntestoq_Click(object sender, EventArgs e)
        {
            UCEstoque tela = new UCEstoque();
            OpenScreen(tela);
            lblLogo.Text = "estoque";

        }
        public void OpenScreen(UserControl control)
        {
            // Se a quantidades de CONTROLES for igual a 1, Ou seja, se já tiver uma tela dentro da GROUP BOX.
            if (pnlConteudo.Controls.Count == 1)
                pnlConteudo.Controls.RemoveAt(0);
            pnlConteudo.Controls.Add(control);
            //Eu irei REMOVER o primeiro ÍNDICE do CONTROLE
            //Ou seja, sempre quando eu for abrir uma tela nova, ele vai remover a tela ATUAL e ADICIONA a NOVA.
        }



        private void FrmTelaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnconsultafornecedor_Click(object sender, EventArgs e)
        {
            UCConsultaFornecedores tela = new UCConsultaFornecedores();
            OpenScreen(tela);
            lblLogo.Text = "Consulta de Fornecedores";
        }

        private void Pagamentos_Click(object sender, EventArgs e)
        {
            UCFolhadePagamento tela = new UCFolhadePagamento();
            OpenScreen(tela);
            lblLogo.Text = "Folha de Pagamento";
        }

        private void lblLogo_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_3(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click_4(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
