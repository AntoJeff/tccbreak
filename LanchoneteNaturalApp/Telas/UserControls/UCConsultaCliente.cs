﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Cliente;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCConsultaCliente : UserControl
    {
        public UCConsultaCliente()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClienteBusiness busi = new ClienteBusiness();
            List<ClienteDTO> consulta = busi.Consulta();

            dgvconsultacliente.AutoGenerateColumns = false;
            dgvconsultacliente.DataSource = consulta;

        }
    }
}
