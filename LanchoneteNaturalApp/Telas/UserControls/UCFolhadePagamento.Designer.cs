﻿namespace LanchoneteNaturalApp.Telas.UserControls
{
    partial class UCFolhadePagamento
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblcargo = new System.Windows.Forms.Label();
            this.btnemitir = new System.Windows.Forms.Button();
            this.cbfuncionario = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbltotalliquido = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbltotalbruto = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblvt = new System.Windows.Forms.Label();
            this.lblvr = new System.Windows.Forms.Label();
            this.lblconvenio = new System.Windows.Forms.Label();
            this.lblim = new System.Windows.Forms.Label();
            this.lblfgts = new System.Windows.Forms.Label();
            this.lblinss = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblcargo);
            this.groupBox3.Controls.Add(this.btnemitir);
            this.groupBox3.Controls.Add(this.cbfuncionario);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(3, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(660, 70);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Buscar Funionario";
            // 
            // lblcargo
            // 
            this.lblcargo.AutoSize = true;
            this.lblcargo.Font = new System.Drawing.Font("Poor Richard", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcargo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblcargo.Location = new System.Drawing.Point(319, 29);
            this.lblcargo.Name = "lblcargo";
            this.lblcargo.Size = new System.Drawing.Size(52, 19);
            this.lblcargo.TabIndex = 28;
            this.lblcargo.Text = "Cargo";
            // 
            // btnemitir
            // 
            this.btnemitir.Location = new System.Drawing.Point(560, 22);
            this.btnemitir.Name = "btnemitir";
            this.btnemitir.Size = new System.Drawing.Size(94, 33);
            this.btnemitir.TabIndex = 27;
            this.btnemitir.Text = "Emitir";
            this.btnemitir.UseVisualStyleBackColor = true;
            this.btnemitir.Click += new System.EventHandler(this.btnemitir_Click);
            // 
            // cbfuncionario
            // 
            this.cbfuncionario.FormattingEnabled = true;
            this.cbfuncionario.Location = new System.Drawing.Point(103, 25);
            this.cbfuncionario.Name = "cbfuncionario";
            this.cbfuncionario.Size = new System.Drawing.Size(139, 27);
            this.cbfuncionario.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(261, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 19);
            this.label3.TabIndex = 24;
            this.label3.Text = "Cargo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 19);
            this.label1.TabIndex = 23;
            this.label1.Text = "Funcionario";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbltotalliquido);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lbltotalbruto);
            this.groupBox2.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(6, 347);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(660, 56);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Total";
            // 
            // lbltotalliquido
            // 
            this.lbltotalliquido.AutoSize = true;
            this.lbltotalliquido.Font = new System.Drawing.Font("Arial Narrow", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotalliquido.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltotalliquido.Location = new System.Drawing.Point(553, 22);
            this.lbltotalliquido.Name = "lbltotalliquido";
            this.lbltotalliquido.Size = new System.Drawing.Size(48, 23);
            this.lbltotalliquido.TabIndex = 27;
            this.lbltotalliquido.Text = "Total";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(414, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 19);
            this.label7.TabIndex = 26;
            this.label7.Text = "Salario Liquido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 19);
            this.label4.TabIndex = 24;
            this.label4.Text = "Salario Bruto:";
            // 
            // lbltotalbruto
            // 
            this.lbltotalbruto.AutoSize = true;
            this.lbltotalbruto.Font = new System.Drawing.Font("Poor Richard", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotalbruto.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltotalbruto.Location = new System.Drawing.Point(121, 22);
            this.lbltotalbruto.Name = "lbltotalbruto";
            this.lbltotalbruto.Size = new System.Drawing.Size(52, 22);
            this.lbltotalbruto.TabIndex = 25;
            this.lbltotalbruto.Text = "Total";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblvt);
            this.groupBox1.Controls.Add(this.lblvr);
            this.groupBox1.Controls.Add(this.lblconvenio);
            this.groupBox1.Controls.Add(this.lblim);
            this.groupBox1.Controls.Add(this.lblfgts);
            this.groupBox1.Controls.Add(this.lblinss);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(660, 227);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informaçoes";
            // 
            // lblvt
            // 
            this.lblvt.AutoSize = true;
            this.lblvt.Font = new System.Drawing.Font("Poor Richard", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvt.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblvt.Location = new System.Drawing.Point(472, 61);
            this.lblvt.Name = "lblvt";
            this.lblvt.Size = new System.Drawing.Size(76, 33);
            this.lblvt.TabIndex = 36;
            this.lblvt.Text = "Total";
            // 
            // lblvr
            // 
            this.lblvr.AutoSize = true;
            this.lblvr.Font = new System.Drawing.Font("Poor Richard", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvr.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblvr.Location = new System.Drawing.Point(260, 61);
            this.lblvr.Name = "lblvr";
            this.lblvr.Size = new System.Drawing.Size(76, 33);
            this.lblvr.TabIndex = 35;
            this.lblvr.Text = "Total";
            // 
            // lblconvenio
            // 
            this.lblconvenio.AutoSize = true;
            this.lblconvenio.Font = new System.Drawing.Font("Poor Richard", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblconvenio.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblconvenio.Location = new System.Drawing.Point(7, 61);
            this.lblconvenio.Name = "lblconvenio";
            this.lblconvenio.Size = new System.Drawing.Size(76, 33);
            this.lblconvenio.TabIndex = 34;
            this.lblconvenio.Text = "Total";
            // 
            // lblim
            // 
            this.lblim.AutoSize = true;
            this.lblim.Font = new System.Drawing.Font("Poor Richard", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblim.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblim.Location = new System.Drawing.Point(472, 138);
            this.lblim.Name = "lblim";
            this.lblim.Size = new System.Drawing.Size(76, 33);
            this.lblim.TabIndex = 33;
            this.lblim.Text = "Total";
            // 
            // lblfgts
            // 
            this.lblfgts.AutoSize = true;
            this.lblfgts.Font = new System.Drawing.Font("Poor Richard", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfgts.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblfgts.Location = new System.Drawing.Point(260, 138);
            this.lblfgts.Name = "lblfgts";
            this.lblfgts.Size = new System.Drawing.Size(76, 33);
            this.lblfgts.TabIndex = 32;
            this.lblfgts.Text = "Total";
            // 
            // lblinss
            // 
            this.lblinss.AutoSize = true;
            this.lblinss.Font = new System.Drawing.Font("Poor Richard", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblinss.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblinss.Location = new System.Drawing.Point(7, 148);
            this.lblinss.Name = "lblinss";
            this.lblinss.Size = new System.Drawing.Size(76, 33);
            this.lblinss.TabIndex = 31;
            this.lblinss.Text = "Total";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Poor Richard", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label14.Location = new System.Drawing.Point(473, 110);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 28);
            this.label14.TabIndex = 30;
            this.label14.Text = "I. Mensal";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Poor Richard", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(257, 110);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 28);
            this.label13.TabIndex = 29;
            this.label13.Text = "FGTS";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Poor Richard", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(6, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 28);
            this.label12.TabIndex = 28;
            this.label12.Text = "INSS";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Poor Richard", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(473, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 28);
            this.label11.TabIndex = 27;
            this.label11.Text = "V. T.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Poor Richard", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(261, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 28);
            this.label10.TabIndex = 26;
            this.label10.Text = "V.R";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Poor Richard", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(6, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 28);
            this.label9.TabIndex = 25;
            this.label9.Text = "Convenio";
            // 
            // UCFolhadePagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "UCFolhadePagamento";
            this.Size = new System.Drawing.Size(666, 431);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblcargo;
        private System.Windows.Forms.Button btnemitir;
        private System.Windows.Forms.ComboBox cbfuncionario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbltotalliquido;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbltotalbruto;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblvt;
        private System.Windows.Forms.Label lblvr;
        private System.Windows.Forms.Label lblconvenio;
        private System.Windows.Forms.Label lblim;
        private System.Windows.Forms.Label lblfgts;
        private System.Windows.Forms.Label lblinss;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}
