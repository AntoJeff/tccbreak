﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Funcionarios;
using LanchoneteNaturalApp.BD.Classes.Funcionario;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCCadastrofuncionario : UserControl
    {
        public UCCadastrofuncionario()
        {
            InitializeComponent();
        }

        private void UCCadastrofuncionario_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        { 
            int telefone = Convert.ToInt32(txttelefone.Text); 
            int cpf = Convert.ToInt32(txtcpf.Text);
            int hora = Convert.ToInt32(txthora.Text);
            float valor = (float)Convert.ToDouble(txtsalario.Text);
            FuncionariosDTO dto = new FuncionariosDTO();
            dto.Nome = txtnome.Text;
            dto.Usuario = txtusuario.Text;
            dto.Senha = txtsenha.Text;
            dto.Rg = txtrg.Text;
            dto.Cpf = cpf;
            dto.Dia = Convert.ToInt32(cbdia.SelectedItem);
            dto.Mes = Convert.ToInt32(cbmes.SelectedItem);
            dto.Ano = Convert.ToInt32(cbano.SelectedItem);
            dto.Endereco = txtendereço.Text;
            dto.Complemento = txtcomplemento.Text;
            dto.Telefone = telefone;
            dto.Cargo = "aa";
            dto.Salario = valor;
            dto.Hora = hora;
            FuncionarioBusiness busi = new FuncionarioBusiness();
            busi.Salvar(dto);
            MessageBox.Show("Ok", "Funcionario", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int telefone = Convert.ToInt32(txttelefone.Text);
            int cpf = Convert.ToInt32(txtcpf.Text);
            FuncionariosDTO dto = new FuncionariosDTO();
            dto.Nome = txtnome.Text;
            dto.Usuario = txtusuario.Text;
            dto.Senha = txtsenha.Text;
            dto.Rg = txtrg.Text;
            dto.Cpf = cpf;
            dto.Dia = Convert.ToInt32(cbdia.SelectedItem);
            dto.Mes = Convert.ToInt32(cbmes.SelectedItem);
            dto.Ano = Convert.ToInt32(cbano.SelectedItem);
            dto.Endereco = txtendereço.Text;
            dto.Complemento = txtcomplemento.Text;
            dto.Telefone = telefone;
            FuncionarioBusiness busi = new FuncionarioBusiness();
            busi.Alterar(dto);
            MessageBox.Show("Alterado com Sucesso!","Funcionario",MessageBoxButtons.OK,MessageBoxIcon.Information);

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
