﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes;
using LanchoneteNaturalApp.BD.Classes.Estoque;
using LanchoneteNaturalApp.BD.Classes.Cardapio;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCCadastroCardapio : UserControl
    {
        public UCCadastroCardapio()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void UCCadastroProduto_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int valor = Convert.ToInt32(txtvalor.Text);
            CardapioDTO dto = new CardapioDTO();       
            dto.Produto = txtproduto.Text;
            dto.Tipo = cbtipo.SelectedText;
            dto.Valor = valor;
            CardapioBusiness busi = new CardapioBusiness();
            busi.Salvar(dto);
            MessageBox.Show("Ok","Cardapio",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}
