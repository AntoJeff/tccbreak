﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Cardapio;
using LanchoneteNaturalApp.BD.Classes.Estoque;
using LanchoneteNaturalApp.BD.Classes;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCEstoque : UserControl
    {
        public UCEstoque()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EstoqueBusiness busi = new EstoqueBusiness();
            List<EstoqueView> View = busi.View();
            dgvCardapio.AutoGenerateColumns = false;
            dgvCardapio.DataSource = View;

        }
    }
}
