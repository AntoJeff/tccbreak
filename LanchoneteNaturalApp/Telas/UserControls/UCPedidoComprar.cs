﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes;
using LanchoneteNaturalApp.BD.Classes.Estoque;
using LanchoneteNaturalApp.BD.Classes.Cardapio;
using LanchoneteNaturalApp.BD.Classes.Fornecedor;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCPedidoComprar : UserControl
    {
        public UCPedidoComprar()
        {
            InitializeComponent();
            ComboProduto();
            ComboFornecedor();
            lbl();
        }

        private void ComboProduto()
        {
            CardapioBusiness busi = new CardapioBusiness();
            List<CardapioDTO> Listar = busi.Listar();
            cbproduto.ValueMember = nameof(CardapioDTO.Id_cardapio);
            cbproduto.DisplayMember = nameof(CardapioDTO.Produto);
            cbproduto.DataSource = Listar;          

        } 
        private void ComboFornecedor()
        {
            FornecedorBusiness busi = new FornecedorBusiness();
            List<FornecedorDTO> Listar = busi.Listar();
            cbfornecedor.ValueMember = nameof(FornecedorDTO.Id_fornecedor);
            cbfornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cbfornecedor.DataSource = Listar;
        }
       public void lbl()
        {
            //CardapioDTO aa = cbproduto.SelectedItem as CardapioDTO;
            //string a = Convert.ToString(aa.Valor);
            //lblvalor.Text = a;

           
        }
        private void UCPedidoComprar_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        } 
        public void CBFornecedor()
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CardapioDTO id = cbproduto.SelectedItem as CardapioDTO;
            FornecedorDTO di = cbfornecedor.SelectedItem as FornecedorDTO;
            //decimal valor = Convert.ToDecimal(txtvalor.Text);
            int quant = Convert.ToInt32(txtvalor.Text);
            EstoqueDTO dto = new EstoqueDTO();
            dto.Id_cardapio = id.Id_cardapio;
            dto.Valor = id.Valor;
            dto.Quantidade = quant;
            dto.Fornecedor = di.Nome;
            EstoqueBusiness busi = new EstoqueBusiness();
            busi.Salvar(dto);
            MessageBox.Show("Ok!", "Compra Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
        }
    }
}
