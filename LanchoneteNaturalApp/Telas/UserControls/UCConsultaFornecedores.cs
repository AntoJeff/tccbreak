﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Fornecedor;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCConsultaFornecedores : UserControl
    {
        public UCConsultaFornecedores()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedorBusiness busi = new FornecedorBusiness();
            List<FornecedorDTO> listar = busi.Listar();

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = listar;
        }
    }
}
