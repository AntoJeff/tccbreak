﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Funcionario;
using LanchoneteNaturalApp.BD.Classes.Cargo;
using LanchoneteNaturalApp.BD.Classes.Funcionarios;
using LanchoneteNaturalApp.BD.Objetos;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCFolhadePagamento : UserControl
    {
        public UCFolhadePagamento()
        {
            InitializeComponent();
            ComboFuncionario();
        }
        public void ComboFuncionario()
        {
            FuncionarioBusiness busi = new FuncionarioBusiness();
            List<FuncionariosDTO> lista = busi.Listar();

            cbfuncionario.ValueMember = nameof(FuncionariosDTO.Id_funcionario);
            cbfuncionario.DisplayMember = nameof(FuncionariosDTO.Nome);
            cbfuncionario.DataSource = lista;
        }


        private void btnemitir_Click(object sender, EventArgs e)
        {
            FuncionariosDTO dto = cbfuncionario.SelectedItem as FuncionariosDTO;  
            //Calculo salario = new Calculo();
            Beneficios beneficios = new Beneficios();
            FGTS fgts = new FGTS();
            INSS inss = new INSS();
            float Fg=  fgts.Fgts(dto.Salario);
            float ins = inss.Inss(dto.Salario);
            float bene = beneficios.beneficios(dto.Salario);
            //float total = salario.Salarios(dto.Hora,dto.Salario);
            float aaa =dto.Salario - ins;
            float bbb = aaa - Fg;
            float ccc = bbb - bene;
            lblfgts.Text = "0.08%";
            lblconvenio.Text = "0.07%";
            lblinss.Text = "0.08%";
            lblvt.Text = "0.06%";
            lblvr.Text = "0.03%";
            lblim.Text = "null";
            lblcargo.Text = "Operacional";
            lblim.Text = "0.2%";
            string bruto = Convert.ToString(dto.Salario);
            lbltotalbruto.Text = bruto;
            string lbl = Convert.ToString(ccc);
            lbltotalliquido.Text = lbl;
            
        }
    }
}
