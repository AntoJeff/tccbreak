﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes;
using LanchoneteNaturalApp.BD.Classes.Estoque;
using LanchoneteNaturalApp.BD.Classes.Cardapio;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCBaixaEstoque : UserControl
    {
        public UCBaixaEstoque()
        {
            InitializeComponent();
            //ComboCardapio();
            Lblvalor();
            Cb();
        }

        private void ComboCardapio()
        {
            CardapioBusiness busi = new CardapioBusiness();
            List<CardapioDTO> Listar = busi.Listar();
            cbproduto.ValueMember = nameof(CardapioDTO.Id_cardapio);
            cbproduto.DisplayMember = nameof(CardapioDTO.Produto);
            cbproduto.DataSource = Listar;

        } 

        private void Cb()
        {
            EstoqueBusiness busi = new EstoqueBusiness();
            List<EstoqueDTO> Listar = busi.Listar();
            cbproduto.ValueMember = nameof(EstoqueDTO.Id_estoque);
            cbproduto.DisplayMember = nameof(EstoqueDTO.Id_cardapio);
            cbproduto.DataSource = Listar;
        }
        public void Lblvalor()
        {
            //CardapioDTO dt = cbproduto.SelectedValue as CardapioDTO;
            //lblvalor.Text = Convert.ToString(dt.Valor);
        }
            
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            EstoqueDTO dt = cbproduto.SelectedItem as EstoqueDTO;
            double Baixa = Convert.ToDouble(txtbaixa.Text);
            EstoqueDTO dto = new EstoqueDTO();
            dto.Id_cardapio = dt.Id_cardapio;
            dto.Quantidade = Baixa;
            EstoqueBusiness busi = new EstoqueBusiness();
            busi.Baixa(dto);
            MessageBox.Show("ok!");
        }
    }
}
