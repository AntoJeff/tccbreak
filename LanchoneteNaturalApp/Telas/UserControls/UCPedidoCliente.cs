﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Pedido;
using LanchoneteNaturalApp.BD.Classes.Estoque;
using LanchoneteNaturalApp.BD.Classes;
using LanchoneteNaturalApp.Telas.Frms;
using LanchoneteNaturalApp.BD.Classes.Cardapio;
using LanchoneteNaturalApp.BD.Classes.Cliente;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCPedidoCliente : UserControl
    {
        BindingList<CardapioDTO> produtosCarrinho = new BindingList<CardapioDTO>();

        public UCPedidoCliente()
        {
            InitializeComponent();
            ComboProduto();
            ComboboxCliente();
        }  
        void Grig()
        {
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = produtosCarrinho;
        }

        private void ComboProduto()
        {
            CardapioBusiness busi = new CardapioBusiness();
            List<CardapioDTO> lista = busi.Listar();

            cbproduto.ValueMember = nameof(CardapioDTO.Id_cardapio);
            cbproduto.DisplayMember = nameof(CardapioDTO.Produto);
            cbproduto.DataSource = lista;
        } 
        private void ComboboxCliente()
        {
            ClienteBusiness busi = new ClienteBusiness();
            List<ClienteDTO> Listar = busi.Consulta();
            cbcliente.ValueMember = nameof(ClienteDTO.Id_cliente);
            cbcliente.DisplayMember = nameof(ClienteDTO.Nome);
            cbcliente.DataSource = Listar;
        }

        private void button1_Click(object sender, EventArgs e)
        {
          

        }

        private void cbproduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            CardapioDTO dto = cbproduto.SelectedItem as CardapioDTO;
            ClienteDTO tdo = cbcliente.SelectedItem as ClienteDTO; 

            int qtd = Convert.ToInt32(txtquantidade.Text);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = produtosCarrinho;

            
            


        }

        private void cbproduto_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            int qtd = Convert.ToInt32(txtquantidade.Text);
            ClienteDTO tdo = cbcliente.SelectedItem as ClienteDTO;
            PedidoDTO dot = new PedidoDTO();
            dot.Id_cliente = tdo.Id_cliente;
            dot.Observacao = txtobservacao.Text;
            dot.Pagamento = cbpagamento.SelectedText;
            dot.Quantidade = qtd;
            PedidoBusiness busi = new PedidoBusiness();
            busi.Salvar(dot);
            MessageBox.Show("Ok!");
            
        }

        private void cbCliente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void UCPedidoCliente_Load(object sender, EventArgs e)
        {

        }
    }
}
